/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/reboot.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <signal.h>

namespace
{
    bool is_pid(const std::string & s)
    {
        return std::string::npos == s.find_first_not_of("0123456789");
    }

    void kill_everybody(const int sig)
    {
        DIR * const dirh = opendir("/proc");
        if (! dirh)
        {
            std::cerr << "Couldn't opendir /proc: " << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cerr << "Sending signal " << sig << " to: ";
        const dirent * dp;
        while (((dp = readdir(dirh))))
        {
            if (! is_pid(dp->d_name))
                continue;

            const pid_t pid = std::atoi(dp->d_name);
            if (1 == pid || getpid() == pid)
                continue;

            std::cerr << pid << " ";
            kill(pid, sig);
        }
        closedir(dirh);
        std::cerr << "\n";
    }

    void remount_root_ro()
    {
        std::cerr << "Remounting / read-only" << std::endl;
        if (0 != mount("none", "/", "none", MS_RDONLY | MS_REMOUNT, ""))
        {
            std::cerr << "Couldn't remount / read-only: " << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    void sync_filesystems()
    {
        std::cerr << "Syncing filesystems" << std::endl;
        sync();
        sleep(1);
        std::cerr << "Syncing filesystems again" << std::endl;
        sync();
    }

    void do_reboot()
    {
        std::cerr << "Rebooting" << std::endl;
        if (0 != reboot(RB_AUTOBOOT))
        {
            std::cerr << "Couldn't call reboot syscall" << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    void power_off()
    {
        std::cerr << "Power off" << std::endl;
        if (0 != reboot(RB_POWER_OFF))
        {
            std::cerr << "Couldn't call reboot syscall" << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

int main(int argc, char * argv[])
{
    bool reboot(false);
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " (--reboot or --poweroff)" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string cmd(argv[1]);
    if (cmd == "--reboot")
        reboot = true;
    else if (cmd == "--poweroff")
        ;
    else
    {
        std::cerr << "Usage: " << argv[0] << " (--reboot or --poweroff)" << std::endl;
        exit(EXIT_FAILURE);
    }

    kill_everybody(SIGTERM);
    sleep(1);
    kill_everybody(SIGKILL);
    sleep(1);
    remount_root_ro();
    sync_filesystems();
    if (reboot)
        do_reboot();
    else
        power_off();
    return EXIT_SUCCESS;
}

